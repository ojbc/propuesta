import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-importbank',
  templateUrl: './importbank.component.html',
  styleUrls: ['./importbank.component.css']
})
export class ImportbankComponent implements OnInit {


    countryForm: FormGroup;
    countries = ['USA', 'Canada', 'Uk']
    constructor(private fb: FormBuilder) {}
    ngOnInit() {
        this.countryForm = this.fb.group({
            countryControl: ['Canada']
        });
    }

}
