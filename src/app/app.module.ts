import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

import { TemplateComponent } from './template/template.component';
import { BankComponent } from './bank/bank.component';
import { ImportbankComponent } from './importbank/importbank.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { VigilanceComponent } from './vigilance/vigilance.component';
const routes: Route[] = [ {path: '', component: LoginComponent}, {path: 'home', component: TemplateComponent}, { path: 'bank/register', component: BankComponent}, { path: 'bank/Import', component: ImportbankComponent}, { path: 'security/user', component: UsersComponent}, { path: 'security/rol', component: RolesComponent}, { path: 'security/permission', component: PermissionsComponent}, { path: 'profile', component: ProfilesComponent}, { path: 'security/vigilance', component: VigilanceComponent}];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
      TemplateComponent,
      BankComponent,
      ImportbankComponent,
      PermissionsComponent,
      RolesComponent,
      UsersComponent,
      ProfilesComponent,
      VigilanceComponent,
  ],
  imports: [
    BrowserModule,
      FormsModule,
      RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
